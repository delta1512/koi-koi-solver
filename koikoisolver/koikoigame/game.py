import random
from enum import Enum
from operator import attrgetter
from typing import Tuple, List, Generator, Optional

from koikoisolver.koikoigame import cards, yaku
from koikoisolver.koikoigame.cards import Card, CardType
from koikoisolver.koikoigame.player import Player


"""
A set of attribute mappings which lead to bool attrs. The scope is expected
to be the `self` of a KoiKoiRound object.

If there is a NOT at the start of the name of the enum, it is expected to
inverse the outcome of the predicate used.

The reason why I am doing this is so that I don't have to write a function
that satisfies each predicate. Rather I only need to reference what's needed.
Hence this can be used to make a very rudimentary boolean satisfier.
"""
GameStatePredicates = {
    "NOT_P1_HAS_CARDS": "p1.hand",
    "P1_HAS_CARDS": "p1.hand",
    "NOT_P2_HAS_CARDS": "p2.hand",
    "P2_HAS_CARDS": "p2.hand",
    "P1_HAS_YAKU": "p1.ext_yaku_flag",
    "NOT_P1_HAS_YAKU": "p1.ext_yaku_flag",
    "P2_HAS_YAKU": "p2.ext_yaku_flag",
    "NOT_P2_HAS_YAKU": "p2.ext_yaku_flag",
    "KOI_STATE": "koi",
    "NOT_KOI_STATE": "koi",
    "STOP_STATE": "stop",
    "NOT_STOP_STATE": "stop",
}


class GameState(Enum):
    """
    Each enum is represented by (id, Tuple[set of conjunctions] of disjunctions)
    """

    PLAYER_1 = (1, ({"P1_HAS_CARDS", "NOT_STOP_STATE"},))
    PLAYER_2 = (2, ({"P2_HAS_CARDS", "NOT_STOP_STATE"},))
    P1_KOI_OR_STOP = (3, ({"P1_HAS_CARDS", "P1_HAS_YAKU"},))
    P2_KOI_OR_STOP = (4, ({"P2_HAS_CARDS", "P2_HAS_YAKU"},))
    # For when you must choose which drawn card to match with
    P1_PICK_DRAW = (5, tuple())
    P2_PICK_DRAW = (6, tuple())
    P1_WIN = (
        7,
        (
            {"P1_HAS_CARDS", "P1_HAS_YAKU", "STOP_STATE"},
            {"NOT_P1_HAS_CARDS", "P1_HAS_YAKU"},
        ),
    )
    P2_WIN = (
        8,
        (
            {"P2_HAS_CARDS", "P2_HAS_YAKU", "STOP_STATE"},
            {"NOT_P2_HAS_CARDS", "P2_HAS_YAKU"},
        ),
    )
    DRAW = (
        9,
        (
            {
                "NOT_P1_HAS_CARDS",
                "NOT_P2_HAS_CARDS",
                "NOT_P1_HAS_YAKU",
                "NOT_P2_HAS_YAKU",
            },
        ),
    )

    @property
    def disjunctions(self):
        return self.value[1]


# A directed graph representing the possible next states for a particular current state
GameStateGraph = {
    GameState.PLAYER_1: frozenset({GameState.P1_PICK_DRAW}),
    GameState.P1_PICK_DRAW: frozenset(
        {GameState.P1_WIN, GameState.P1_KOI_OR_STOP, GameState.PLAYER_2}
    ),
    GameState.P1_WIN: frozenset(),
    GameState.P1_KOI_OR_STOP: frozenset({GameState.PLAYER_2, GameState.P1_WIN}),
    GameState.PLAYER_2: frozenset({GameState.P2_PICK_DRAW}),
    GameState.P2_PICK_DRAW: frozenset(
        {GameState.P2_WIN, GameState.P2_KOI_OR_STOP, GameState.PLAYER_1, GameState.DRAW}
    ),
    GameState.P2_WIN: frozenset(),
    GameState.P2_KOI_OR_STOP: frozenset({GameState.PLAYER_1, GameState.P2_WIN}),
    GameState.DRAW: frozenset(),
}


# Constants that help aggregate states
# Came cannot continue
GAME_END = frozenset({GameState.P1_WIN, GameState.P2_WIN, GameState.DRAW})
# Waiting for a player to choose a matching option from a drawn card
PLAYER_PICK_DRAW = frozenset({GameState.P1_PICK_DRAW, GameState.P2_PICK_DRAW})
# Any state where get_possibe_plays should be used
MUST_PLAY_STATE = frozenset(
    {
        GameState.PLAYER_1,
        GameState.PLAYER_2,
        GameState.P1_PICK_DRAW,
        GameState.P2_PICK_DRAW,
    }
)
# Easy way to figure out who's turn it is
PLAYER_1_TURN = frozenset(
    {
        GameState.PLAYER_1,
        GameState.P1_PICK_DRAW,
        GameState.P1_KOI_OR_STOP,
        GameState.P1_WIN,
    }
)
PLAYER_2_TURN = frozenset(
    {
        GameState.PLAYER_2,
        GameState.P2_PICK_DRAW,
        GameState.P2_KOI_OR_STOP,
        GameState.P2_WIN,
    }
)
# Basically everything except DRAW
PLAYER_STATE = frozenset(PLAYER_1_TURN | PLAYER_2_TURN)
# Waiting for either player to koi or stop
KOI_OR_STOP = frozenset({GameState.P1_KOI_OR_STOP, GameState.P2_KOI_OR_STOP})


class KoiKoiRound:
    def __init__(self, players: Tuple[Player] = None):
        self.deck: List[Card] = list(Card.get_deck())
        random.shuffle(self.deck)

        if players is None:
            self.p1 = Player("P1")
            self.p1.new_game_setup(self.deck)
            self.p2 = Player("P2")
            self.p2.new_game_setup(self.deck)
        else:  # Determine oya through shuffle
            tmp_players = list(players.copy())
            random.shuffle(tmp_players)
            self.p1, self.p2 = tmp_players

        self.board = [self.deck.pop() for i in range(8)]

        # Cronological order of states and moves
        self.history: List[Tuple[GameState, Optional[Tuple[Card, Card]]]] = [
            (GameState.PLAYER_1,)
        ]

        self.koi = False
        self.stop = False

    @property
    def state(self):
        """The current game state is abstracted as the last state in the history"""
        return self.history[-1][0]

    @property
    def current_player(self):
        """The current player abstracted as a method to determine which player's turn it is"""
        if self.state in PLAYER_1_TURN:
            return self.p1

        elif self.state in PLAYER_2_TURN:
            return self.p2

        elif self.state == GameState.DRAW:
            # If we can't tell from above, go back until we can
            for history in reversed(self.history):
                last_state = history[0]

                if last_state in PLAYER_1_TURN:
                    return self.p1
                elif last_state in PLAYER_2_TURN:
                    return self.p2

        assert False, "If you are seeing this, I programmed this part wrong."

    def get_possible_plays(self) -> Tuple[Tuple[Card]]:
        """
        Returns the possible moves that can be made. Each move is a tuple with length
        of either 1 or 2. The first item representing what came out of the player's hand
        and the second representing what was matched on the board.

        If the game is in the P*_PICK_DRAW state, then the first item represents what
        comes out of the deck.
        """
        # If the game is in the win state or waiting for koi or stop, return nothing
        if self.state in GAME_END or self.state in KOI_OR_STOP:
            return tuple()

        if self.state in PLAYER_PICK_DRAW:
            possible_matches = tuple(cards.find_matches(self.deck[-1], self.board))

            if len(possible_matches) == 0:
                return ((self.deck[-1],),)
            else:
                return possible_matches

        # After selecting the player, find all possible matches and any discards
        # To filter out impossible discards (ones that will match if placed on the
        # board), we compare them to the months on the board. If a month exists,
        # then we should not expect a discard.
        possible_matches = sum(
            [list(cards.find_matches(c, self.board)) for c in self.current_player.hand],
            start=[],
        )
        used_months = {c.month for c in self.board}
        possible_discards = [
            (c,) for c in self.current_player.hand if c.month not in used_months
        ]

        return tuple(possible_matches + possible_discards)

    def get_next_state(self) -> GameState:
        # Grab the immediate neighbours of the current state
        next_states = GameStateGraph[self.state]

        # If there is only one, that will be the next state
        if len(next_states) == 1:
            # next_states is a frozenset
            return next(iter(next_states))

        # Check for Yaku only after picking a card to draw
        # This flag will be held up until we decide the next state is a player state
        if self.state in PLAYER_PICK_DRAW:
            self.current_player.ext_yaku_flag = self.current_player.has_new_yaku()

        # Otherwise pick the first one that has all predicates satisfied
        default = None
        for s in next_states:
            if len(s.disjunctions) == 0:
                default = s
                continue

            for djs in s.disjunctions:
                # Perform the conjunction
                satisfied = all(
                    # Perform the controlled-NOT
                    map(
                        lambda t: bool(t[0]) ^ t[1],
                        # Put together the predicate and whether it needs to be flipped
                        zip(
                            # Get all referenced attributes from the predicates amd resolve them
                            attrgetter(*map(GameStatePredicates.get, djs))(self),
                            # Get ready to align the predicates with whether they need a bit-flip
                            [pred.startswith("NOT") for pred in djs],
                        ),
                    )
                )

                if satisfied:
                    # We shall lower the flag if we know that the player is continuing
                    if s in {GameState.PLAYER_1, GameState.PLAYER_2}:
                        self.current_player.ext_yaku_flag = False

                    return s

        assert not default is None, f"{self.state} is terminal."

        return default

    def finish_turn(self, play: Tuple[Card] = None) -> GameState:
        """
        Progresses the game to the next state after any necessary operations have been
        completed in the current state.
        """
        if play is None:
            play = tuple()

        next_state = self.get_next_state()

        self.history.append((next_state, play))

        self.koi = False
        self.stop = False

        return self.state

    def make_move(self, play: Tuple[Card]) -> GameState:
        """
        Makes a move and progresses the round. To decide KOI or STOP, use the
        koi or stop method.
        """
        assert not (
            self.state in GAME_END or self.state in KOI_OR_STOP
        ), "Can't play when in game ended or koi/stop"
        player_drawing = self.state in PLAYER_PICK_DRAW
        play_is_match = len(play) == 2

        # Handle the movement of cards
        # Drawing a card always removes from the deck
        if player_drawing:
            self.deck.pop()
        else:
            self.current_player.play_card(play[0])

        # Matches always remove a cardfrom the board
        if play_is_match:
            self.board.pop(self.board.index(play[1]))
            self.current_player.do_match(play)
        # Otherwise we place something onto the board
        else:
            self.board.append(play[0])

        return self.finish_turn(play)

    def call_koi(self) -> GameState:
        assert self.state in KOI_OR_STOP, "Can't call koi or stop when still playing"
        self.current_player.kois += 1
        self.koi = True

        return self.finish_turn()

    def call_stop(self) -> GameState:
        assert self.state in KOI_OR_STOP, "Can't call koi or stop when still playing"
        self.stop = True

        return self.finish_turn()
