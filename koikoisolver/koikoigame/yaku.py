import os
from enum import Enum
from typing import List

from koikoisolver.koikoigame.cards import Card, CardType


class YakuKoiKoiJapan(Enum):
    """
    Format is (card type, number of cards, points)
    """

    LIGHTS_5 = (CardType.LIGHT, 5, 10)
    LIGHTS_4 = (CardType.LIGHT, 4, 8)
    # NOTE: Rainy Four lights is expected to be handled using logic. This enum should only be used for points
    RAINY_LIGHTS_4 = (None, 4, 7)
    LIGHTS_3 = (CardType.LIGHT, 3, 5)

    BOAR_DEER_BUTTERFLY = (CardType.BDB, 3, 5)
    TANE = (CardType.SEED, 5, 1)

    AKATAN = (CardType.POETRY_RIBBON, 3, 5)
    AOTAN = (CardType.BLUE_RIBBON, 3, 5)
    TAN = (CardType.RIBBON, 5, 1)

    MOON_VIEWING = (CardType.MOON, 2, 1)
    CHERRY_VIEWING = (CardType.CHERRY, 2, 1)

    JUNK = (CardType.JUNK, 10, 1)

    @property
    def type(self):
        return self.value[0]

    @property
    def req_cards(self):
        return self.value[1]

    @property
    def points(self):
        return self.value[2]

    def __lt__(self, other):
        return self.value < other.value


class YakuTraditional(Enum):
    """
    Format is (card type, number of cards, points)
    """

    LIGHTS_5 = (CardType.LIGHT, 5, 15)
    LIGHTS_4 = (CardType.LIGHT, 4, 8)
    # NOTE: Rainy Four lights is expected to be handled using logic. This enum should only be used for points
    RAINY_LIGHTS_4 = (None, 4, 7)
    LIGHTS_3 = (CardType.LIGHT, 3, 6)

    BOAR_DEER_BUTTERFLY = (CardType.BDB, 3, 5)
    TANE = (CardType.SEED, 5, 1)

    AKATAN = (CardType.POETRY_RIBBON, 3, 5)
    AOTAN = (CardType.BLUE_RIBBON, 3, 5)
    TAN = (CardType.RIBBON, 5, 1)

    MOON_VIEWING = (CardType.MOON, 2, 5)
    CHERRY_VIEWING = (CardType.CHERRY, 2, 5)

    JUNK = (CardType.JUNK, 10, 1)

    @property
    def type(self):
        return self.value[0]

    @property
    def req_cards(self):
        return self.value[1]

    @property
    def points(self):
        return self.value[2]

    def __lt__(self, other):
        return self.value < other.value


# Select which set of yaku to use by checking the env
if os.environ.get("YAKU") == "Traditional":
    YAKU_SET = YakuTraditional
else:
    YAKU_SET = YakuKoiKoiJapan


def find_yaku(matches: List[Card], yaku_set: Enum = YAKU_SET) -> List[Enum]:
    """
    Provided a list of cards that make up the matches, separate them into
    bins and figure out whether or not yaku have been created.

    :returns: List of yaku enums which represent yaku found in the matches
    """
    bins = {y: list() for y in CardType}
    yaku = list()

    for card in matches:  # For every card
        for card_type in card.value[1]:  # Sort them into bins based on type
            bins[card_type].append(card)

    for y in yaku_set:
        # Handle the lights
        if y.type is None:
            if Card.RAIN_MAN in bins[CardType.LIGHT] and len(bins[CardType.LIGHT]) == 4:
                yaku.append(yaku_set.RAINY_LIGHTS_4)
            continue

        # Cannot only get 3/4 lights if rain man is not in the cards list
        if (
            y in {yaku_set.LIGHTS_4, yaku_set.LIGHTS_3}
            and Card.RAIN_MAN in bins[CardType.LIGHT]
        ):
            continue

        if y.type == CardType.LIGHT:
            if len(bins[CardType.LIGHT]) == y.value[1]:
                yaku.append(y)
            continue

        n_cards = len(bins[y.type])  # Figure out how many cards are in
        yaku_n_cards = y.value[1]

        # Add the yaku to the list for the full match and every additional card
        # Score will be calculated as: for every unique Yaku, take the points value,
        # then for every additional duplicate, add 1 point.
        for i in range(n_cards - yaku_n_cards + 1):
            yaku.append(y)

    return yaku
