from typing import List, Tuple

from koikoisolver.koikoigame import yaku
from koikoisolver.koikoigame.cards import Card


class Player:
    def __init__(self, name: str = "P1"):
        self.name = name
        self.hand = []
        self.matches = []
        self.score = 0
        self.kois = 0
        self.last_yaku = tuple()
        # This variable is to be used externally in conjunction with the satisfier in game.py.
        # This is because we may need to store yaku states across states or checks.
        self.ext_yaku_flag = False

    def new_game_setup(self, deck: List[Card]):
        # pick some cards from the deck
        self.hand = [deck.pop() for i in range(8)]
        # Reset some defaults
        self.kois = 0
        self.last_yaku = tuple()
        self.ext_yaku_flag = False
        self.matches = []

    def play_card(self, card: Card):
        """Handles the removal of a card from the hand"""
        self.hand.pop(self.hand.index(card))

    def do_match(self, match: Tuple[Card]):
        """Assimilates a pair of matched cards."""
        self.matches += list(match)

    def has_new_yaku(self) -> bool:
        new_yaku = yaku.find_yaku(self.matches)

        if len(new_yaku) != len(self.last_yaku):
            self.last_yaku = new_yaku
            return True

        return False
