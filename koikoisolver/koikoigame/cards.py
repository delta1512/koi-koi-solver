from enum import Enum, IntEnum
from typing import List, Tuple, Generator
from collections import defaultdict
from itertools import combinations


class Months(IntEnum):
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12


class CardType(IntEnum):
    LIGHT = 9
    BLUE_RIBBON = 8
    POETRY_RIBBON = 7
    BDB = 6  # Board-Deer-ButterFly
    MOON = 5
    CHERRY = 4
    RIBBON = 3
    SEED = 2
    JUNK = 1


class Card(Enum):
    """
    Ordered as (month, card types, number of duplicates)
    """

    JAN_JUNK = (Months.JANUARY, (CardType.JUNK,), 2)
    JAN_POETRY_RIBBON = (Months.JANUARY, (CardType.RIBBON, CardType.POETRY_RIBBON), 1)
    CRANE = (Months.JANUARY, (CardType.LIGHT,), 1)

    BUSH_WARBLER = (Months.FEBRUARY, (CardType.SEED,), 1)
    FEB_POETRY_RIBBON = (Months.FEBRUARY, (CardType.RIBBON, CardType.POETRY_RIBBON), 1)
    FEB_JUNK = (Months.FEBRUARY, (CardType.JUNK,), 2)

    CURTAIN = (Months.MARCH, (CardType.LIGHT, CardType.CHERRY), 1)
    MAR_POETRY_RIBBON = (Months.MARCH, (CardType.RIBBON, CardType.POETRY_RIBBON), 1)
    MAR_JUNK = (Months.MARCH, (CardType.JUNK,), 2)

    CUCKOO = (Months.APRIL, (CardType.SEED,), 1)
    APR_PLAIN_RIBBON = (Months.APRIL, (CardType.RIBBON,), 1)
    APR_JUNK = (Months.APRIL, (CardType.JUNK,), 2)

    BRIDGE = (Months.MAY, (CardType.SEED,), 1)
    MAY_PLAIN_RIBBON = (Months.MAY, (CardType.RIBBON,), 1)
    MAY_JUNK = (Months.MAY, (CardType.JUNK,), 2)

    BUTTERFLY = (Months.JUNE, (CardType.SEED, CardType.BDB), 1)
    JUN_BLUE_RIBBON = (Months.JUNE, (CardType.RIBBON, CardType.BLUE_RIBBON), 1)
    JUN_JUNK = (Months.JUNE, (CardType.JUNK,), 2)

    BOAR = (Months.JULY, (CardType.SEED, CardType.BDB), 1)
    JUL_PLAIN_RIBBON = (Months.JULY, (CardType.RIBBON,), 1)
    JUL_JUNK = (Months.JULY, (CardType.JUNK,), 2)

    MOON = (Months.AUGUST, (CardType.LIGHT, CardType.MOON), 1)
    GOOSE = (Months.AUGUST, (CardType.SEED,), 1)
    AUG_JUNK = (Months.AUGUST, (CardType.JUNK,), 2)

    SAKE_CUP = (
        Months.SEPTEMBER,
        (CardType.SEED, CardType.JUNK, CardType.MOON, CardType.CHERRY),
        1,
    )
    SEP_BLUE_RIBBON = (Months.SEPTEMBER, (CardType.RIBBON, CardType.BLUE_RIBBON), 1)
    SEP_JUNK = (Months.SEPTEMBER, (CardType.JUNK,), 2)

    DEER = (Months.OCTOBER, (CardType.SEED, CardType.BDB), 1)
    OCT_BLUE_RIBBON = (Months.OCTOBER, (CardType.RIBBON, CardType.BLUE_RIBBON), 1)
    OCT_JUNK = (Months.OCTOBER, (CardType.JUNK,), 2)

    RAIN_MAN = (Months.NOVEMBER, (CardType.LIGHT,), 1)
    SWALLOW = (Months.NOVEMBER, (CardType.SEED,), 1)
    NOV_PLAIN_RIBBON = (Months.NOVEMBER, (CardType.RIBBON,), 1)
    NOV_JUNK = (Months.NOVEMBER, (CardType.JUNK,), 1)

    PHOENIX = (Months.DECEMBER, (CardType.LIGHT,), 1)
    DEC_JUNK = (Months.DECEMBER, (CardType.JUNK,), 3)

    @property
    def month(self):
        return self.value[0]

    @property
    def types(self):
        return self.value[1]

    @property
    def occurances(self):
        return self.value[2]

    @classmethod
    def get_deck(cls) -> Generator[Enum, None, None]:
        for c in cls:
            for _ in range(c.occurances):
                yield c

    def __lt__(self, other):
        return self.value < other.value


def find_matches(
    card: Card, match_list: List[Card]
) -> Generator[Tuple[Card, Card], None, None]:
    """
    Compares a single card to a list of cards and returns the pair which matches. The `card`
    will always be the first element of the resulting tuple
    """
    for c in match_list:
        if card.month == c.month:
            yield (card, c)


def generate_all_possible_matches() -> Generator[Tuple[Card, Card], None, None]:
    """
    Generates all possible matches in a standard koi koi deck.

    Does this in an efficient way by building a dictionary and then iterating within each month.
    """
    card_months = defaultdict(lambda: [])

    for card in Card.get_deck():
        card_months[card.month].append(card)

    for cards in card_months.values():
        for match in combinations(cards, 2):
            yield match
