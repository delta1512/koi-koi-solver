import unittest
import itertools as it

from koikoisolver.koikoigame import yaku
from koikoisolver.koikoigame.cards import Card, CardType
from koikoisolver.util.text_display import format_yaku


class TestYakuKKJ(unittest.TestCase):
    def test_3_lights(self):
        possible_lights = it.combinations(
            filter(lambda c: CardType.LIGHT in c.types, Card), 3
        )

        for matches in possible_lights:
            curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)

            if Card.RAIN_MAN in matches:
                exp_yaku = []
            else:
                exp_yaku = [yaku.YakuKoiKoiJapan.LIGHTS_3]

            self.assertListEqual(
                exp_yaku,
                curr_yaku,
                msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
            )

    def test_4_lights(self):
        possible_lights = it.combinations(
            filter(lambda c: CardType.LIGHT in c.types, Card), 4
        )

        for matches in possible_lights:
            curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)

            if Card.RAIN_MAN in matches:
                exp_yaku = [yaku.YakuKoiKoiJapan.RAINY_LIGHTS_4]
            else:
                exp_yaku = [yaku.YakuKoiKoiJapan.LIGHTS_4]

            self.assertListEqual(
                exp_yaku,
                curr_yaku,
                msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
            )

    def test_5_lights(self):
        all_lights = list(filter(lambda c: CardType.LIGHT in c.types, Card))

        curr_yaku = yaku.find_yaku(all_lights, yaku.YakuKoiKoiJapan)
        self.assertListEqual([yaku.YakuKoiKoiJapan.LIGHTS_5], curr_yaku)

    def test_bdb(self):
        boar_deer_butterfly = list(filter(lambda c: CardType.BDB in c.types, Card))

        curr_yaku = yaku.find_yaku(boar_deer_butterfly, yaku.YakuKoiKoiJapan)
        self.assertListEqual([yaku.YakuKoiKoiJapan.BOAR_DEER_BUTTERFLY], curr_yaku)

    def test_5_seed(self):
        possible_seeds = it.combinations(
            filter(lambda c: CardType.SEED in c.types, Card), 5
        )
        boar_deer_butterfly = set(filter(lambda c: CardType.BDB in c.types, Card))

        for matches in possible_seeds:
            curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)
            exp_yaku = [yaku.YakuKoiKoiJapan.TANE]

            # If board-deer-butterfly is in the matches, add it
            if boar_deer_butterfly & set(matches) == boar_deer_butterfly:
                exp_yaku.append(yaku.YakuKoiKoiJapan.BOAR_DEER_BUTTERFLY)

            # Order and duplicates matter, hence we can't use a set-equality test
            exp_yaku.sort()
            curr_yaku.sort()

            self.assertListEqual(
                exp_yaku,
                curr_yaku,
                msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
            )

    def test_gt5_seed(self):
        boar_deer_butterfly = set(filter(lambda c: CardType.BDB in c.types, Card))

        # There are a total of 9 seed, we want combinations for everything >6
        for n_seed in range(6, 10):
            possible_seeds = it.combinations(
                filter(lambda c: CardType.SEED in c.types, Card), n_seed
            )

            for matches in possible_seeds:
                curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)
                exp_yaku = [yaku.YakuKoiKoiJapan.TANE for i in range(n_seed - 4)]

                # If board-deer-butterfly is in the matches, add it
                if boar_deer_butterfly & set(matches) == boar_deer_butterfly:
                    exp_yaku.append(yaku.YakuKoiKoiJapan.BOAR_DEER_BUTTERFLY)

                # Order and duplicates matter, hence we can't use a set-equality test
                exp_yaku.sort()
                curr_yaku.sort()

                self.assertListEqual(
                    exp_yaku,
                    curr_yaku,
                    msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
                )

    def test_akatan(self):
        akatan = list(filter(lambda c: CardType.POETRY_RIBBON in c.types, Card))

        curr_yaku = yaku.find_yaku(akatan, yaku.YakuKoiKoiJapan)
        self.assertListEqual([yaku.YakuKoiKoiJapan.AKATAN], curr_yaku)

    def test_aotan(self):
        aotan = list(filter(lambda c: CardType.BLUE_RIBBON in c.types, Card))

        curr_yaku = yaku.find_yaku(aotan, yaku.YakuKoiKoiJapan)
        self.assertListEqual([yaku.YakuKoiKoiJapan.AOTAN], curr_yaku)

    def test_5_tan(self):
        possible_ribbons = it.combinations(
            filter(lambda c: CardType.RIBBON in c.types, Card), 5
        )
        akatan = set(filter(lambda c: CardType.POETRY_RIBBON in c.types, Card))
        aotan = set(filter(lambda c: CardType.BLUE_RIBBON in c.types, Card))

        for matches in possible_ribbons:
            curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)
            exp_yaku = [yaku.YakuKoiKoiJapan.TAN]

            # If akatan or aotan is in the matches, then add it
            if akatan & set(matches) == akatan:
                exp_yaku.append(yaku.YakuKoiKoiJapan.AKATAN)
            elif aotan & set(matches) == aotan:
                exp_yaku.append(yaku.YakuKoiKoiJapan.AOTAN)

            # Order and duplicates matter, hence we can't use a set-equality test
            exp_yaku.sort()
            curr_yaku.sort()

            self.assertListEqual(
                exp_yaku,
                curr_yaku,
                msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
            )

    def test_gt5_tan(self):
        akatan = set(filter(lambda c: CardType.POETRY_RIBBON in c.types, Card))
        aotan = set(filter(lambda c: CardType.BLUE_RIBBON in c.types, Card))

        # There are a total of 10 ribbons, we want combinations for everything >6
        for n_tan in range(6, 11):
            possible_ribbons = it.combinations(
                filter(lambda c: CardType.RIBBON in c.types, Card), n_tan
            )

            for matches in possible_ribbons:
                curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)
                exp_yaku = [yaku.YakuKoiKoiJapan.TAN for i in range(n_tan - 4)]

                # If akatan or aotan is in the matches, then add it
                if akatan & set(matches) == akatan:
                    exp_yaku.append(yaku.YakuKoiKoiJapan.AKATAN)

                if aotan & set(matches) == aotan:
                    exp_yaku.append(yaku.YakuKoiKoiJapan.AOTAN)

                # Order and duplicates matter, hence we can't use a set-equality test
                exp_yaku.sort()
                curr_yaku.sort()

                self.assertListEqual(
                    exp_yaku,
                    curr_yaku,
                    msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
                )

    def test_viewing(self):
        all_viewings = {
            (Card.MOON, Card.SAKE_CUP): [yaku.YakuKoiKoiJapan.MOON_VIEWING],
            (Card.CURTAIN, Card.SAKE_CUP): [yaku.YakuKoiKoiJapan.CHERRY_VIEWING],
            (Card.CURTAIN, Card.MOON, Card.SAKE_CUP): [
                yaku.YakuKoiKoiJapan.MOON_VIEWING,
                yaku.YakuKoiKoiJapan.CHERRY_VIEWING,
            ],
        }

        for matches, exp_yaku in all_viewings.items():
            curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)
            curr_yaku.sort()
            exp_yaku.sort()

            self.assertListEqual(
                curr_yaku,
                exp_yaku,
                msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
            )

    def test_10_junk(self):
        possible_junk = it.combinations(
            filter(lambda c: CardType.JUNK in c.types, Card), 10
        )

        for matches in possible_junk:
            curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)
            exp_yaku = [yaku.YakuKoiKoiJapan.JUNK]

            # Order and duplicates matter, hence we can't use a set-equality test
            exp_yaku.sort()
            curr_yaku.sort()

            self.assertListEqual(
                exp_yaku,
                curr_yaku,
                msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
            )

    def test_gt10_junk(self):
        junk = set(filter(lambda c: CardType.JUNK in c.types, Card))

        # There are a total of 25 junk, we want combinations for everything >10
        for n_junk in range(11, 26):
            possible_junk = it.combinations(
                filter(lambda c: CardType.JUNK in c.types, Card), n_junk
            )

            for matches in possible_junk:
                curr_yaku = yaku.find_yaku(matches, yaku.YakuKoiKoiJapan)
                exp_yaku = [yaku.YakuKoiKoiJapan.JUNK for i in range(n_junk - 9)]

                # Order and duplicates matter, hence we can't use a set-equality test
                exp_yaku.sort()
                curr_yaku.sort()

                self.assertListEqual(
                    exp_yaku,
                    curr_yaku,
                    msg=f"{format_yaku(exp_yaku)}\n{format_yaku(curr_yaku)}",
                )
