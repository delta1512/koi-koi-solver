import unittest

from koikoisolver.algo.basic_min_max import select_next_match, Card, Match

from koikoisolver.koikoigame.cards import Card as StdCard
from koikoisolver.koikoigame.cards import generate_all_possible_matches


class TestBasicMinMax(unittest.TestCase):
    def test_card_scoring(self):
        card = Card(StdCard.JAN_JUNK)
        self.assertEqual(card.score, 1)

        card = Card(StdCard.JAN_POETRY_RIBBON)
        self.assertEqual(card.score, 8)

        card = Card(StdCard.CRANE)
        self.assertEqual(card.score, 9)

        card = Card(StdCard.SAKE_CUP)
        self.assertEqual(card.score, 8)

    def test_match_scoring(self):
        match = Match([Card(StdCard.JAN_JUNK), Card(StdCard.JAN_JUNK)])
        self.assertEqual(match.score, 2)

        match = Match([Card(StdCard.JAN_JUNK), Card(StdCard.JAN_POETRY_RIBBON)])
        self.assertEqual(match.score, 9)

        match = Match([Card(StdCard.JAN_JUNK), Card(StdCard.CRANE)])
        self.assertEqual(match.score, 10)

        match = Match([Card(StdCard.CRANE), Card(StdCard.JAN_POETRY_RIBBON)])
        self.assertEqual(match.score, 17)

    def test_select_next_match(self):
        matches = generate_all_possible_matches()
        match = select_next_match(matches)
        self.assertEqual(match.score, 18)
        self.assertListEqual(
            [StdCard.MAR_POETRY_RIBBON, StdCard.CURTAIN], list(sorted(match.std_match))
        )
