import random
import unittest

from koikoisolver.koikoigame import cards, game
from koikoisolver.koikoigame.cards import Card


class TestMatching(unittest.TestCase):
    def test_all_matching(self):
        for i in cards.Months:
            current_month = [c for c in Card if c.month == i]

            for c in current_month:
                self.assertTrue(
                    set(cards.find_matches(c, list(Card)))
                    == {(c, mc) for mc in current_month}
                )

    def test_none_matching(self):
        march = (c for c in Card if c.month == 3)
        crane = Card.CRANE

        self.assertTrue(len(list(cards.find_matches(crane, march))) == 0)

    def test_random_matches(self):
        for i in range(10000):
            # Choose a sample without replacement from all cards
            curr_cards = random.sample(list(Card), random.randint(1, 16))
            # Grab a random card to try and match against
            curr_card = curr_cards.pop()

            self.assertSetEqual(
                set(cards.find_matches(curr_card, curr_cards)),
                {
                    (curr_card, oc)
                    for oc in filter(lambda c: c.month == curr_card.month, curr_cards)
                },
            )
