import unittest
import random

from koikoisolver.koikoigame import cards, game
from koikoisolver.koikoigame.cards import Card


class TestGame(unittest.TestCase):
    def test_no_missing_cards(self):
        kkg = game.KoiKoiRound()

        all_game_cards = set(kkg.board + kkg.deck + kkg.p1.hand + kkg.p2.hand)
        all_cards = set(Card.get_deck())

        self.assertTrue(all_game_cards == all_cards)

    def test_cards_length(self):
        kkg = game.KoiKoiRound()

        all_game_cards = kkg.board + kkg.deck + kkg.p1.hand + kkg.p2.hand
        all_cards = list(Card.get_deck())

        all_game_cards.sort()
        all_cards.sort()

        self.assertEqual(all_cards, all_game_cards)

    def test_possible_plays(self):
        # Because of randomness, run this a few times
        for i in range(1000):
            kkg = game.KoiKoiRound()

            months_matched = set()
            plays = kkg.get_possible_plays()

            # Check the possible pairings
            for m in filter(lambda x: len(x) == 2, plays):
                months_matched.add(m[0].month)
                # Cards are a match if they have the same month
                self.assertTrue(m[0].month == m[1].month)

            # For cards that do not match, then they should not be apart of the detected months
            for m in filter(lambda x: len(x) == 1, plays):
                self.assertTrue(m[0].month not in months_matched)

    def test_random_rounds(self):
        """
        Simply runs a few random rounds until completion to see if there are any
        exceptions or weird behaviour.
        """
        # Because of randomness, run this a few times
        for i in range(1000):
            kkg = game.KoiKoiRound()

            while kkg.state not in game.GAME_END:
                plays = kkg.get_possible_plays()

                kkg.make_move(random.choice(plays))

                if kkg.state in game.KOI_OR_STOP:
                    if i % 2 == 0:
                        kkg.call_stop()
                    else:
                        kkg.call_koi()

                # Conservation of cards must remain true after every move
                self.assertTrue(
                    len(kkg.p1.hand)
                    + len(kkg.p1.matches)
                    + len(kkg.p2.hand)
                    + len(kkg.p2.matches)
                    + len(kkg.board)
                    + len(kkg.deck)
                    == 48
                )

    def test_next_state(self):
        pass
