import unittest

from koikoisolver.koikoigame import cards
from koikoisolver.koikoigame.cards import Card


class TestCards(unittest.TestCase):
    def test_find_simple_matches(self):
        card = Card.DEC_JUNK
        other_cards = (
            Card.PHOENIX,
            Card.DEC_JUNK,
            Card.BOAR,
            Card.APR_JUNK,
            Card.NOV_JUNK,
            Card.CRANE,
            Card.SWALLOW,
        )

        answer = [(card, Card.PHOENIX), (card, Card.DEC_JUNK)]
        answer.sort()

        self.assertListEqual(
            list(sorted(cards.find_matches(card, other_cards))), answer
        )

    def test_get_deck(self):
        deck = list(Card.get_deck())

        self.assertEqual(len(deck), 48)

        for c in Card:
            self.assertEqual(deck.count(c), c.occurances)

    def test_generate_all_possible_matches(self):
        for match in cards.generate_all_possible_matches():
            self.assertEqual(match[0].month, match[1].month)
