"""
A basic min-max algorithm for Koikoi.
"""

from koikoisolver.koikoigame.cards import Card as StdCard

from typing import Tuple


class Card:
    def __init__(self, card: StdCard):
        self.card = card

    @property
    def score(self):
        return max(self.card.types) + len(self.card.types) - 1

    def __lt__(self, other):
        """
        A card's value is the maximal card type plus 1 for every other type.
        """
        return self.score < other.score


class Match:
    def __init__(self, match: Tuple[Card]):
        self.match = match

    @property
    def score(self):
        return sum([c.score for c in self.match]) if len(self.match) == 2 else 0

    @property
    def std_match(self) -> Tuple[StdCard]:
        return tuple([c.card for c in self.match])

    def __lt__(self, other):
        """
        A match's value is the sum of the card's values
        """
        return self.score < other.score


def select_next_match(matches: Tuple[Tuple[StdCard]]):
    return max([Match([Card(c) for c in m]) for m in matches])
