from koikoisolver.koikoigame import game

import random


def run(n=10000):
    p1_wins = 0
    p2_wins = 0
    draws = 0

    for _ in range(n):
        kkg = game.KoiKoiRound()

        while kkg.state not in game.GAME_END:
            plays = kkg.get_possible_plays()

            kkg.make_move(random.choice(plays))

            if kkg.state in game.KOI_OR_STOP:
                kkg.call_stop()

            if kkg.state in game.GAME_END:
                if kkg.state == game.GameState.P1_WIN:
                    p1_wins += 1
                elif kkg.state == game.GameState.P2_WIN:
                    p2_wins += 1
                else:
                    draws += 1

    print(f"P1 wins: {p1_wins}")
    print(f"P2 wins: {p2_wins}")
    print(f"Draws: {draws}")
    print(f"P1 win rate: {p1_wins / n}")
