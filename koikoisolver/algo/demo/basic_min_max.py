from koikoisolver.koikoigame import game
from koikoisolver.algo.basic_min_max import select_next_match
from koikoisolver.parsers import kkj

import random


def run(n=10000):
    p1_wins = 0
    p2_wins = 0
    draws = 0

    for _ in range(n):
        kkg = game.KoiKoiRound()

        while kkg.state not in game.GAME_END:
            plays = kkg.get_possible_plays()

            if kkg.state in game.PLAYER_1_TURN:
                kkg.make_move(select_next_match(plays).std_match)
            else:
                kkg.make_move(random.choice(plays))

            if kkg.state in game.KOI_OR_STOP:
                kkg.call_stop()

            if kkg.state in game.GAME_END:
                if kkg.state == game.GameState.P1_WIN:
                    p1_wins += 1
                elif kkg.state == game.GameState.P2_WIN:
                    p2_wins += 1
                else:
                    draws += 1

    print(f"P1 wins: {p1_wins}")
    print(f"P2 wins: {p2_wins}")
    print(f"Draws: {draws}")
    print(f"P1 win rate: {p1_wins / n}")


def run_real(path: str):
    board_cards, hand_cards = kkj.parse(path)

    kkg = game.KoiKoiRound()
    kkg.board = list(board_cards)
    kkg.p1.hand = list(hand_cards)

    plays = kkg.get_possible_plays()
    print(select_next_match(plays).std_match)
