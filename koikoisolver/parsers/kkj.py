import koikoisolver
from koikoisolver.util.card_extraction import extract_cards
from koikoisolver.util import cv as kkcv
from koikoisolver.koikoigame.cards import Card

from typing import Optional
import tempfile
import os
import glob

import cv2


CARDS_PATH_PNG = os.path.join(koikoisolver.__path__[0], "koikoigame", "card_images", "kkj", "png")


def parse(game_image_path: str):
    with tempfile.TemporaryDirectory() as temp_dir:
        extract_cards(game_image_path, temp_dir)

        # Now split them into the board and hand lists
        board_files = glob.glob(os.path.join(temp_dir, 'board_*'))
        hand_files = glob.glob(os.path.join(temp_dir, 'hand_*'))

        board_cards = [image_to_hanafuda(i, 10) for i in board_files]
        hand_cards = [image_to_hanafuda(i, 10) for i in hand_files]

    not_none = lambda x: x is not None
    return filter(not_none, board_cards), filter(not_none, hand_cards)


def image_to_hanafuda(image_path: str, thresh: float=0.25) -> Optional[Card]:
    all_card_image_paths = glob.glob(os.path.join(CARDS_PATH_PNG, "*.png"))

    assert len(all_card_image_paths) > 0, f"No cards found in the module hierarchy, make sure they are present in {CARDS_PATH_PNG}"

    best_card: Card = None
    best_distance: float = float('inf')

    for card_image_path in all_card_image_paths:
        distance = kkcv.compare_two_cards_mse(image_path, card_image_path)

        if distance < best_distance:
            best_card_name = os.path.splitext(os.path.basename(card_image_path))[0].split("-")[0]
            best_card = getattr(Card, best_card_name)
            best_distance = distance

    if best_distance < thresh:
        return best_card


