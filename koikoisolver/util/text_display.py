from enum import Enum
from pprint import pformat
from collections import Counter
from typing import List

from koikoisolver.koikoigame.cards import Card, CardType


def extract_name_from_enum(e: Enum):
    """
    Simply finds the period in the __str__() method and grabs everything after it.
    """
    s = e.__str__()
    return s[s.index(".") + 1 :]


def format_hand(hand: List[Card]) -> str:
    nice_hand = {extract_name_from_enum(ct): list() for ct in CardType}

    for c in hand:
        for ct in c.types:
            nice_hand[extract_name_from_enum(ct)].append(extract_name_from_enum(c))

    return pformat(nice_hand)


def format_yaku(yaku: List[Enum]) -> str:
    if len(yaku) == 0:
        return "{}"

    # Access the brand of yaku being used
    yaku_type = type(yaku[0])
    nice_yaku = dict(Counter((extract_name_from_enum(y) for y in yaku)))

    return pformat(nice_yaku)
