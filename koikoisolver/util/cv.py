import cv2
import numpy as np

def compare_two_cards_contours(card_path_1: str, card_path_2: str, method=cv2.CONTOURS_MATCH_I1, c_method=cv2.CHAIN_APPROX_SIMPLE, c_mode=cv2.RETR_EXTERNAL) -> float:
    contours_1 = get_contours_for_image(card_path_1, c_mode, c_method)
    contours_2 = get_contours_for_image(card_path_2, c_mode, c_method)

    return cv2.matchShapes(contours_1, contours_2, method, 0.0)

def get_contours_for_image(image_path: str, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    _, thresh = cv2.threshold(image, 127, 255, 0)
    _, contours = cv2.findContours(thresh, mode, method)

    return contours[0].astype('float32')


def compare_two_cards_mse(card_path_1: str, card_path_2: str) -> float:
    image1 = cv2.imread(card_path_1, cv2.IMREAD_GRAYSCALE)
    image2 = cv2.imread(card_path_2, cv2.IMREAD_GRAYSCALE)

    return np.mean((image1 - image2)**2)