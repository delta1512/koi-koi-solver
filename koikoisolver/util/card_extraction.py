'''
This util extracts the cards from the center board and the current player's hand from Koi Koi Japan on Steam.
This is modified code generated from ChatGPT 3.5.
'''
import os

from PIL import Image

def extract_cards(image_path, output_folder):
    image = Image.open(image_path)
    
    # Extract cards from the board section
    board_start = (405, 237)
    board_end = (795, 483) # NOTE: Might want to add 80 pixels to x
    card_size = (70, 118)
    card_spacing = 10
    extract_cards_from_section(image, board_start, board_end, card_size, card_spacing, output_folder, "board")

    # Extract cards from your hand
    hand_start = (290, 596)
    hand_end = (990, 714)
    card_spacing_hand = 20
    extract_cards_from_section(image, hand_start, hand_end, card_size, card_spacing_hand, output_folder, "hand")

def extract_cards_from_section(image, section_start, section_end, card_size, card_spacing, output_folder, section_name):
    x, y = section_start
    card_width, card_height = card_size
    
    while x <= section_end[0] and y <= section_end[1]:
        card = image.crop((x, y, x + card_width, y + card_height))
        card.save(os.path.join(output_folder, f"{section_name}_card_{x}_{y}.png"))
        x += card_width + card_spacing
        
        if x + card_width > section_end[0]:
            x = section_start[0]
            y += card_height + card_spacing